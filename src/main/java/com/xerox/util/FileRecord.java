package com.xerox.util;

public class FileRecord
{
  public String filename;
  public String originalfilePath;
  public String newfilename;
  public String headerString;
  public FileRecord(String fileName, String originalFilePath, String newFileName, String header)
  {
	  filename = fileName;
	  originalfilePath = originalFilePath;
	  newfilename = newFileName;
	  headerString = header;
  }
}