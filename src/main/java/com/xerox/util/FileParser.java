package com.xerox.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import org.apache.commons.io.FileUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FileParser {
	private static Log log = LogFactory.getFactory().getInstance(FileParser.class);
	//creates a ping message
	public String createPingMessage(String fileLoc)
	{
		String messageText = null;
		BufferedReader read = null;
		try{
			 read = new BufferedReader(new FileReader(fileLoc));
			 String line = null;
			 while( (line = read.readLine()) != null) {
			       	StringTokenizer tokens = new StringTokenizer(line);
			       	String record = tokens.nextToken();
					if(!record.startsWith("*"))
					{
			    	  messageText = record.substring(0, record.length());
			    	  read.close();
					  break;
					}
			   	}
			 }catch (Exception e){
			     if( log.isErrorEnabled() ) {
						log.error("Error while creating Ping Message" + e.getMessage());
					}
			  	}
		return messageText;
	}
	public String parseFile2(String fileLoc)
	{
		String messageText = "";
		   try{
			   
			   return FileUtils.readFileToString(new File(fileLoc), "UTF-8");
			   }catch (Exception e){
				   if( log.isErrorEnabled() ) {
						log.error("Error while parsing File" + e.getMessage());
					}
			  }
		return messageText;
	}
	//creates string message
	public String createStringMessage(String fileLoc, int startRec, int endRec) throws IOException
	{
		String messageText = null;
		StringBuilder builder = new StringBuilder();
		InputStream is = new BufferedInputStream(new FileInputStream(fileLoc));
		   try{
			   int count= 1;
			   byte[] c = new byte[350];
			   while(count <= endRec)
			   {
				   int len = is.read(c);
				   if(count >= startRec)
				   {
					//Read the file line and create a string
					 messageText = new String(c);
					 builder.append(messageText);
				   }
				   count++;
			   	}
			   }catch (Exception e){
				   if( log.isErrorEnabled() ) {
						log.error("Error while parsing File" + e.getMessage());
					}
			  }
		   finally{
			   
			   is.close();
		   }
		return builder.toString();
	}
	//counts the number of records in file
	public static int countLines(String filename) throws IOException {
	    InputStream is = new BufferedInputStream(new FileInputStream(filename));
	    try {
	        byte[] c = new byte[350];
	        int count = 0;
	        int readChars = 0;
	        boolean empty = true;
	        while ((readChars = is.read(c)) != -1) {
	            empty = false;
	            for (int i = 0; i < readChars; ++i) {
	                if (c[i] == '\n' || i== 349 ) {
	                    ++count;
	                }
	            }
	        }
	        return (count == 0 && !empty) ? 1 : count;
	    } finally {
	        is.close();
	    }
	}
	//rename the file while it is being processed
	public String renameFile(File originalFile)
	{
		String target = null;
		try{
			String currentExtension = getFileExtension(originalFile.getName());
			
			if (currentExtension.equals("")){
			      target = originalFile.getName() + "." + "tmp";
			   }
			   else {
			      target = originalFile.getName().replaceAll("." + currentExtension, ".tmp");
			   }
			boolean success = originalFile.renameTo(new File(originalFile.getParent(),target));
			if (!success)
			{
				target = null;
			}
		}catch (Exception e){
			   if( log.isErrorEnabled() ) {
					log.error("Error while parsing File" + e.getMessage());
				}
		  }
		return target;
	}
	//get the file extension
	 public static String getFileExtension(String f) {
		   String ext = "";
		   int i = f.lastIndexOf('.');
		   if (i > 0 &&  i < f.length() - 1) {
		      //ext = f.substring(i + 1).toLowerCase();
			   ext = f.substring(i + 1);
		   }
		   return ext;
		}
	 //Move the file once it is processed
	public boolean moveFile(FileRecord rec, File newlocation, File dupe) {
		// move it to the target location
		boolean success = false;
		boolean dupeCondition  = false;
		File originalfile = new File(rec.originalfilePath, rec.newfilename);
		if( log.isDebugEnabled() ) {
			log.debug("Moving File from:" + originalfile.getPath() + " to:" + newlocation.getAbsolutePath());
		}
		if(newlocation.exists())
		{
			success = originalfile.renameTo(new File(newlocation, rec.filename));
			if(!success) {
			    dupeCondition  = true;
				if( log.isErrorEnabled()) {
					log.error("Failed to move file to:" + newlocation.getPath()+", duplicate condition detected or file is locked.");
				}
				File fileUndeDupFolder = new File(new StringBuffer()
						.append(dupe.getAbsolutePath())
						.append("\\")
						.append(rec.filename).toString());
				if(fileUndeDupFolder.exists())
				{
					if( log.isDebugEnabled()) {
						log.debug("File with same name already exits is deleted");
					}
					fileUndeDupFolder.delete();
				}
				if(originalfile.renameTo(new File(dupe, rec.filename)))
				{
					if( log.isDebugEnabled()) {
						log.debug("File: " + originalfile.getPath()+" has been moved to:" + dupe.getPath());
					}
				success = true;
				}
			}
		}
		else
		{
			if( log.isErrorEnabled() ) {
				log.error("Destination folder "+newlocation.getAbsolutePath()+" is unavailable due to Network disconnect, Failed to move file " + originalfile.getPath());
			}
			success = false;
		}
		return success;
	}
	///Writes string to a file
	public void writeToFile(String mqMessage) throws IOException
	{
		FileWriter fw = null;
		BufferedWriter bw = null;
		PrintWriter out = null;
		try {
				fw = new FileWriter("e:\\sample.txt", true);
			    bw = new BufferedWriter(fw);
			    out = new PrintWriter(bw);
			    out.println(mqMessage);
			    out.close();
        } catch (IOException e ) {
            e.printStackTrace();
        } finally {
            
                if(out != null)
                    out.close();
            try {
                if(bw != null)
                    bw.close();
            } catch (IOException e) {
                //exception handling left as an exercise for the reader
            }
            try {
                if(fw != null)
                    fw.close();
            } catch (IOException e) {
                //exception handling left as an exercise for the reader
            }
        }
	}
}