package com.xerox.StringMover;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.TimerTask;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xerox.StringMover.Configurator;
import com.xerox.StringMover.MQMessagingService;
import com.xerox.StringMover.ScanForWork;
import com.xerox.util.FileParser;
import com.xerox.util.FileRecord;

public class ScanForWork extends TimerTask {

	private static Log log = LogFactory.getFactory().getInstance(ScanForWork.class);
	private static MQMessagingService mqms = null;
	private static Hashtable<String,FileRecord> fileList = new Hashtable<String,FileRecord>();

	public ScanForWork() {
		super();
		if( log.isDebugEnabled() ) {
			log.debug("ScanForWork()");	}
		mqms = new MQMessagingService();
		if( mqms == null ) {
			if( log.isErrorEnabled() ) {
				log.error("Failed to initialize MQ Messaging Service.....");
			}
		}
		try {
			if( log.isDebugEnabled() ) {
				log.debug("constructing: " + Configurator.getInstance().getMQ_QUEUE_MANAGER());
				log.debug("constructing: " + Configurator.getInstance().getMQ_UPLOAD_QUEUE());
				log.debug("constructing: " + Configurator.getInstance().getMQ_DOWNLOAD_QUEUE());
				log.debug("constructing: " + Configurator.getInstance().getMQ_CHANNEL());
				log.debug("constructing: " + Configurator.getInstance().getMQ_HOST());
				log.debug("constructing: " + Configurator.getInstance().getMQ_PORT());
				log.debug("constructing: " + Configurator.getInstance().getMQ_USERID());
				log.debug("constructing: " + Configurator.getInstance().getMessage_Size());
			}
			mqms.setQueueManagerName(Configurator.getInstance().getMQ_QUEUE_MANAGER());
			mqms.setMQChannel(Configurator.getInstance().getMQ_CHANNEL());
			mqms.setMQHost(Configurator.getInstance().getMQ_HOST());
			mqms.setMQPort(Configurator.getInstance().getMQ_PORT());
			mqms.setMQUserid(Configurator.getInstance().getMQ_USERID());
		} catch (Exception e) {
			if( log.isErrorEnabled() ) {
				log.error("Failed to configure MQ Messaging Service");
			}
		}
	}
	public void run() {
		if( log.isDebugEnabled() ) {
			log.debug("ScanForWork.run()");
		}
		boolean messageSent = false;
		// check directory for files with the desired extensions
		File getdir = new File(Configurator.getInstance().getWATCH_DIRECTORY());
		File newloc = new File(Configurator.getInstance().getSEND_DIRECTORY());
		File invalid = new File(Configurator.getInstance().getINVLAID_DIRECTORY());
		try
		{
			Collection files = DirectoryScanner.getFilesInDirectoryFIFO(getdir, Configurator.getInstance().getFILE_EXTENSIONS());
			int settleTime= Configurator.getInstance().getFILE_SETTLE_PERIOD();
			if( log.isDebugEnabled() ) {
				log.debug("Sleeping for " + settleTime + " seconds to let files settle before acting upon them...");
			}
			Thread.sleep(1000L * settleTime);
			//---------------------Send Ping Messages--------------------------------------------------------------------//
			FileRecord fileRecord;
			FileRecord hasFile;
			Iterator it = files.iterator();
			while(it.hasNext()) {
				// grab the next one
				File file = (File) it.next();
				String name = file.getName();
				// it is necessary to capture the file size here, before it is moved
				String filelen = Long.toString(file.length());
				if( log.isDebugEnabled() ) {
					log.debug("file length = " + file.length());
				}
				//Check if the message is in the processing list or not and if not add it to the list, so that it will not be processed again in case of failure
				FileParser fileParser= new FileParser();
				//Parse File and create a Ping MQ Message
				String pingMessage = fileParser.createPingMessage(file.getAbsolutePath());
				String msgCor = pingMessage.substring(0, 25);
				hasFile = fileList.get(msgCor);
				//send Ping MQ message if it has not been sent and move the file to new location
				if(hasFile == null)
				{
					messageSent = sendMessage(pingMessage);
					//rename the file to prevent from reading again only if message has been sent successfully
					if(messageSent)
					{
						String  newFileName = fileParser.renameFile(file);
						if(newFileName != null)
						{
							fileRecord = new FileRecord(name, file.getParent(), newFileName, pingMessage);
							fileList.put(msgCor, fileRecord);
							if( log.isDebugEnabled() ) {
									log.debug("File renamed to " + newFileName);
								}
						}
						else
						{
							if( log.isErrorEnabled() ) {
									log.error(">>>>>Failed to move the file<<<<<");}
								}
						}
					else
					{
						
						if( log.isDebugEnabled() ) {
							log.debug("Unable to send ping message to Q:" + pingMessage);
						}
					}
				}
			}
			//-----------------------Receive Ping Responses and send the main file and move the files based on response status-----------------------
			if(!fileList.isEmpty())
			{	
				String receivedMessage = receiveMessage();
				//String receivedMessage = "0122016/05/23091215111830RC=00";
				if(receivedMessage != null)
				{
					String responseMessages[] = receivedMessage.split("\\_");
					if(responseMessages.length > 0)
					{
						for (int i= 0; i <= responseMessages.length-1; i++)
						{
							String message [] = responseMessages[i].split("\\=");
							String messageCorResp = message[0].substring(0, 25);
							String messageStatus = message[1];
							if( log.isDebugEnabled() ) {
									log.debug("Response message from Download Queue & Status:" + receivedMessage);
								}
							processFile(messageCorResp, messageStatus, newloc, invalid);
						}
					}
				}	
				else
				{
					if( log.isDebugEnabled() ) {
							log.debug("No response message received from the Download Queue");
						}
				}
			}
		}
		catch(Exception exp)
		{
			if( log.isErrorEnabled() ) {
				log.error("Error while polling the source folder :"+exp.getMessage());
				}
		}
	}
	
	private boolean sendMessage(String mqMessage) {
		boolean success = true;
		try {
				mqms.setQueueName(Configurator.getInstance().getMQ_UPLOAD_QUEUE());
				mqms.send(mqMessage);
			
		} catch (Exception e) {
			success = false;
			if( log.isErrorEnabled() ) {
				log.error("Failed to send MQ message for " + mqMessage, e);
			}
		}
		return success;
	}
	private String receiveMessage()
	{
		String messageReceived = null;
		try{
			mqms.setQueueName(Configurator.getInstance().getMQ_DOWNLOAD_QUEUE());	
			messageReceived = mqms.receive();
		}catch (Exception e){
			if( log.isErrorEnabled() ) {
				log.error("Failed to receive MQ message", e);
			}
		}
		return messageReceived;
	}
	private void processFile(String messageCorResp, String messageStatus, File newlocation, File invalid) throws IOException
	{
		try
		{
			//get the file name based on msgCor
			FileRecord rec = fileList.get(messageCorResp);
			FileParser fileParser = new FileParser();
			if(rec != null){
				String cycleNum = rec.headerString.substring(2, 3);
				File newfile = new File(rec.originalfilePath, rec.newfilename);
				File newloc = new File(newlocation.getAbsolutePath() + "\\" + cycleNum);

				//Valid record, continue sending the file
				if(messageStatus.contains("00")){
				//Get the file record count
					int recCount = fileParser.countLines(rec.originalfilePath + "\\" + rec.newfilename);
					int msgCount = Integer.parseInt(Configurator.getInstance().getMessage_Size());
					int loopCount = recCount/ msgCount;
					if(recCount % msgCount !=0)
					{
						loopCount = loopCount + 1;
					}
					boolean messageSent = false;
					String mqMessage = null;
					int startIndex = 1;
					int endIndex = msgCount;
					for(int i = 1; i <= loopCount; i++)
					{
						endIndex = msgCount * i;
						if(endIndex > recCount) endIndex = recCount;
						mqMessage = fileParser.createStringMessage(newfile.getAbsolutePath(), startIndex, endIndex);
						if(mqMessage.matches("\089999999999")) mqMessage.replaceFirst("\\s++$", "");
						messageSent = sendMessage(mqMessage);
						//fileParser.writeToFile(mqMessage);
						startIndex = endIndex + 1;
					}
					if(messageSent) {
						fileList.remove(messageCorResp);
						fileParser.moveFile(rec, newloc, invalid);
							}
					else {
							if( log.isErrorEnabled() ) {
								log.error(">>>>>MQ send failed<<<<<");
							}
						}
					}
				//04:error � invalid record - don�t send any more of this string
				//08:error � trailer record not sent - don�t send any more of this string
				//12:error � duplicate entry - don�t send any more of this string.
				else if (messageStatus.contains("04") ||  messageStatus.contains("08") || messageStatus.contains("12")) {
					//remove the record from the processing list
					fileList.remove(messageCorResp);
					//move the file to invalid file location
					boolean success2 = fileParser.moveFile(rec, invalid, invalid);
					if( log.isDebugEnabled() ) {
							log.debug("File Moved to" + invalid.getAbsolutePath() + "\\" + rec.filename);
					}
					if(success2) {
							
						if( log.isErrorEnabled() ) {
								log.error("Moved " + rec.filename + " to Invalid Files Location");
							}
						} else {
							if( log.isErrorEnabled() ) {
								log.error(">>>>> MQ send failed AND file could not be moved back to new position! <<<<<");
								log.error(">>>>> Potential loss of data in file " + rec.filename + " <<<<<");
							}
						}
					}
				else {
						if( log.isErrorEnabled() ) {
							log.error("Invalid Status Received from Download Q" + messageCorResp);
						}
					}
				}
			}
		catch(Exception e)
		{
			log.error("Error Processing the String Message" + e.getMessage());
			
		}
	}
}

