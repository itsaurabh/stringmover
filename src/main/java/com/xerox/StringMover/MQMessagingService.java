package com.xerox.StringMover;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibm.mq.MQC;
import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQGetMessageOptions;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;

public class MQMessagingService implements IMessagingService {
	
	private static Log log = LogFactory.getFactory().getInstance(MQMessagingService.class);
	
	private String mqChannel = null;
	private String mqHost = null;
	private String mqPort = null;
	private String queueMgrName = "";	// can be an empty string 
	private String queueName = null;
	private String mqUserid = null;
	private MQQueueManager qmgr = null;
	private MQQueue queue = null;

	public MQMessagingService() {
		super();
	}

	public void setMQChannel(String channel) throws Exception {
		mqChannel = channel;
	}

	public void setMQHost(String host) throws Exception {
		mqHost = host;
	}

	public void setMQPort(String port) throws Exception {
		mqPort = port;
	}

	public void setQueueManagerName(String qmgrname) throws Exception {
		queueMgrName = qmgrname;
	}
	
	public void setQueueName(String qname) throws Exception {
		queueName = qname;
	}

	public void setMQUserid(String mqUserid) {
		this.mqUserid = mqUserid;
	}

	public void open(int mode) throws Exception {
		try {
			if( log.isDebugEnabled() ) {
				log.debug("Opening Queue...");
			}
			MQEnvironment.hostname = mqHost;
			MQEnvironment.channel = mqChannel;
			MQEnvironment.port = Integer.parseInt(mqPort);
			MQEnvironment.userID = mqUserid;
			MQEnvironment.properties.put(MQC.TRANSPORT_PROPERTY,MQC.TRANSPORT_MQSERIES);
			
			qmgr = new MQQueueManager(queueMgrName);
			//openOption for receiving messages
			//int openOptions = MQC.MQOO_INQUIRE | MQC.MQOO_INPUT_EXCLUSIVE | MQC.MQOO_BROWSE;
			int openOptions = MQC.MQOO_INQUIRE | MQC.MQOO_INPUT_SHARED | MQC.MQOO_BROWSE;
			if( mode == (MQC.MQOO_INPUT_AS_Q_DEF|MQC.MQOO_INQUIRE|MQC.MQOO_OUTPUT))
			{
				//openOption for sending messages
				openOptions = MQC.MQOO_INQUIRE | MQC.MQOO_OUTPUT | MQC.MQOO_FAIL_IF_QUIESCING | MQC.MQOO_BROWSE ;
			}
			queue = qmgr.accessQueue(queueName,openOptions);
			if( log.isDebugEnabled() ) {
				log.debug("Opened" + queueName);
			}
		} catch (MQException e) {
			close();
			log.error("Unable to Open the queue:" + queueName);
			throw new Exception("MQException: Completion code=" + e.completionCode + " Reason code=" + e.reasonCode, e);
		}
	}
	public void close() throws Exception {
		try {
				if( queue != null ) {
					queue.close();
				}
			} catch (MQException e) {
				throw new Exception("MQException: Completion code=" + e.completionCode + " Reason code=" + e.reasonCode, e);
			}
			queue = null;
			if( log.isDebugEnabled() ) {
				log.debug("commit and disconnect qmgr");
			}
			if( qmgr != null ) {
				qmgr.commit();
				qmgr.disconnect();
			}
			qmgr = null;
	}
	public void send(String msg) throws Exception {
		if( log.isDebugEnabled() ) {
			log.debug("sending message started");
		}
		open(MQC.MQOO_INPUT_AS_Q_DEF|MQC.MQOO_INQUIRE|MQC.MQOO_OUTPUT);
		try {
			if( msg.length() > 0 ) {
				MQMessage buf = new MQMessage();
				buf.clearMessage();
				buf.format = MQC.MQFMT_STRING;
				buf.writeString(msg);
				buf.seek(0);		
				MQPutMessageOptions pmo = new MQPutMessageOptions();
				pmo.options += MQC.MQPMO_FAIL_IF_QUIESCING ;
				queue.put(buf, pmo);
				if( log.isDebugEnabled() ) {
					log.debug("Message Sent to Queue " + buf);
					if( buf != null ) {
						log.debug("msgLength: " + buf.getMessageLength());
						log.debug("readString: " + buf.readString(buf.getMessageLength()));
					}
					buf.seek(0);		
				}
			}
		} catch (MQException e) {
			if( log.isErrorEnabled() ) {
				log.error("MQException: Completion code=" + e.completionCode + " Reason code=" + e.reasonCode, e);
			}
			throw new Exception("MQException: Completion code=" + e.completionCode + " Reason code=" + e.reasonCode, e);
		} catch (Exception e) {
			if( log.isErrorEnabled() ) {
				log.error("Exception " + e.getMessage(), e);
			}
		} finally {
			if( log.isDebugEnabled() ) {
				log.debug("in finally... closing...");
			}
			close();
		}
	}
	public String receive() throws Exception {
		if( log.isDebugEnabled() ) {
			log.debug("receiving message");
		}
		StringBuilder sb = new StringBuilder();
		String msgText= null;
		open(MQC.MQOO_INQUIRE | MQC.MQOO_BROWSE);
		try {
				MQMessage buf = new MQMessage();
				MQGetMessageOptions gmo = new MQGetMessageOptions();
				gmo.options=MQC.MQGMO_WAIT | MQC.MQGMO_BROWSE_FIRST;
				gmo.matchOptions=MQC.MQMO_NONE;
				gmo.waitInterval=5000;
				int counter = 1;
				int messages = queue.getCurrentDepth();
				while(counter <= messages)
				{
					messages = queue.getCurrentDepth();
					gmo.options = MQC.MQGMO_WAIT | MQC.MQGMO_BROWSE_NEXT;
					queue.get(buf, gmo);
					byte[] b = new byte[buf.getMessageLength()];
					int msglen = b.length > 32 ? 30 : b.length;
					buf.readFully(b, 0, msglen);
					msgText = new String(b);
					sb.append(msgText.trim());
					sb.append("_");
			        gmo.options = MQC.MQGMO_WAIT | MQC.MQGMO_MSG_UNDER_CURSOR;
			        queue.get(buf, gmo);
			        counter ++;
			        if( log.isInfoEnabled() ) {
						log.info("Message: " + buf.putApplicationName + "|" + buf.putDateTime.getTime() + "|" + buf.readLine());
					}
			        buf.clearMessage();
				}
				if(sb.length() > 0)
				{
					sb.delete(sb.length()-1, sb.length());
					msgText = sb.toString();
				}
				messages = queue.getCurrentDepth();
				if( log.isDebugEnabled() )
					log.debug("Message Received \n\t" + msgText);
		} catch (MQException e) {
			if( e.reasonCode == MQException.MQRC_NO_MSG_AVAILABLE ) {
				if( log.isInfoEnabled() ) {
					log.info("No messages in queue");
				}
			} else {
				throw new Exception("MQException: Completion code=" + e.completionCode + " Reason code=" + e.reasonCode, e);
			}
		} finally {
			if( log.isDebugEnabled() )
				log.debug("in finally... closing...");
			close();
		}
	return msgText;
	}
}
