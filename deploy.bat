mkdir DeploymentScripts\lib\commons-io-1.0
mkdir DeploymentScripts\lib\commons-logging-1.0.3
mkdir DeploymentScripts\lib\jakarta-log4j-1.2.8\dist\lib
mkdir DeploymentScripts\lib\ibm-mq-for-java
mkdir DeploymentScripts\Config
mkdir DeploymentScripts\logs

copy lib\commons-io-1.0\*.jar DeploymentScripts\lib\commons-io-1.0
copy lib\commons-logging-1.0.3\*.jar DeploymentScripts\lib\commons-logging-1.0.3
copy lib\jakarta-log4j-1.2.8\dist\lib\*.jar DeploymentScripts\lib\jakarta-log4j-1.2.8\dist\lib
copy lib\ibm-mq-for-java\*.jar DeploymentScripts\lib\ibm-mq-for-java
copy lib\*.dll DeploymentScripts\lib
copy config\* DeploymentScripts\Config
javac -classpath src\main\java\com\xerox\stringmover\*java src\main\java\com\xerox\util\*.java
jar cf manifest.info DeploymentScripts\lib\StringMover.jar DeploymentScripts\Config\StringMover.properties DeploymentScripts\runStringMover.bat README.txt -C build .